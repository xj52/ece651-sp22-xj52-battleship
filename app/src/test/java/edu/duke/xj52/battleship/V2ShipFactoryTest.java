package edu.duke.xj52.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V2ShipFactoryTest {

    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c : expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
        }
    }

    @Test
    public void test_makeSubmarine() {
        V2ShipFactory f = new V2ShipFactory();
        Ship<Character> ship = f.makeSubmarine(new Placement(new Coordinate(2, 4), 'H'));
        checkShip(ship, "Submarine", 's', new Coordinate(2, 4), new Coordinate(2, 5));
    }

    @Test
    public void test_makeSubmarine_ori_fault() {
        V2ShipFactory f = new V2ShipFactory();
        assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(new Placement(new Coordinate(2, 4), 'z')));
    }

    @Test
    public void test_makeBattleship_ori_fault() {
        V2ShipFactory f = new V2ShipFactory();
        assertThrows(IllegalArgumentException.class, () -> f.makeBattleship(new Placement(new Coordinate(2, 4), 'z')));
    }

    @Test
    public void test_makeBattleship() {
        V2ShipFactory f = new V2ShipFactory();
        Ship<Character> ship = f.makeBattleship(new Placement(new Coordinate("a0"), 'l'));
        checkShip(ship, "Battleship", 'b', new Coordinate("a1"), new Coordinate("b0"), new Coordinate("b1"), new Coordinate("c1"));
    }

    @Test
    public void test_makeCarrier() {
        V2ShipFactory f = new V2ShipFactory();
        Ship<Character> ship = f.makeCarrier(new Placement(new Coordinate("a1"), 'r'));
        checkShip(ship, "Carrier", 'c', new Coordinate("a2"), new Coordinate("a3"), new Coordinate("a4"), new Coordinate("a5"), new Coordinate("b1"), new Coordinate("b2"), new Coordinate("b3"));
    }

    @Test
    public void test_makeDestroyer() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        V2ShipFactory f = new V2ShipFactory();
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    }
}