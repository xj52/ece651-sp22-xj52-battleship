package edu.duke.xj52.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;

import static edu.duke.xj52.battleship.SpecialShapedShip.makeCoords;
import static edu.duke.xj52.battleship.SpecialShapedShip.makeOrder;
import static org.junit.jupiter.api.Assertions.*;

class SpecialShapedShipTest {

    @Test
    public void test_makeCoords_BU() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate("A0"), "Battleship", 'U');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(0, 1));
        expected.add(new Coordinate(1, 0));
        expected.add(new Coordinate(1, 1));
        expected.add(new Coordinate(1, 2));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeCoords_BR() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate("A0"), "Battleship", 'R');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate("A0"));
        expected.add(new Coordinate("b0"));
        expected.add(new Coordinate("c0"));
        expected.add(new Coordinate("b1"));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeCoords_BD() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate("A0"), "Battleship", 'D');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(0, 0));
        expected.add(new Coordinate(0, 1));
        expected.add(new Coordinate(0, 2));
        expected.add(new Coordinate(1, 1));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeCoords_BL() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate("A0"), "Battleship", 'L');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate("a1"));
        expected.add(new Coordinate("b0"));
        expected.add(new Coordinate("b1"));
        expected.add(new Coordinate("c1"));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeCoords_CL() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate("A0"), "Carrier", 'L');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate("A2"));
        expected.add(new Coordinate("A3"));
        expected.add(new Coordinate("A4"));
        expected.add(new Coordinate("B0"));
        expected.add(new Coordinate("B1"));
        expected.add(new Coordinate("B2"));
        expected.add(new Coordinate("B3"));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeCoords_CD() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate("A0"), "Carrier", 'D');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate("A0"));
        expected.add(new Coordinate("b0"));
        expected.add(new Coordinate("c0"));
        expected.add(new Coordinate("b1"));
        expected.add(new Coordinate("c1"));
        expected.add(new Coordinate("d1"));
        expected.add(new Coordinate("e1"));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeOrder_BU() {
        HashMap<Coordinate, Integer> res;
        res = makeOrder(new Coordinate("A1"), "Battleship", 'U');
        HashMap<Coordinate, Integer> expected = new HashMap<>();
        expected.put(new Coordinate("a2"), 1);
        expected.put(new Coordinate("b1"), 2);
        expected.put(new Coordinate("b2"), 3);
        expected.put(new Coordinate("b3"), 4);
        assertEquals(expected, res);
    }

    @Test
    public void test_makeOrder_BR() {
        HashMap<Coordinate, Integer> res;
        res = makeOrder(new Coordinate("A3"), "Battleship", 'R');
        HashMap<Coordinate, Integer> expected = new HashMap<>();
        expected.put(new Coordinate("a3"), 2);
        expected.put(new Coordinate("b3"), 3);
        expected.put(new Coordinate("c3"), 4);
        expected.put(new Coordinate("b4"), 1);
        assertEquals(expected, res);
    }

    @Test
    public void test_makeOrder_BD() {
        HashMap<Coordinate, Integer> res;
        res = makeOrder(new Coordinate("A1"), "Battleship", 'D');
        HashMap<Coordinate, Integer> expected = new HashMap<>();
        expected.put(new Coordinate("a1"), 4);
        expected.put(new Coordinate("a2"), 3);
        expected.put(new Coordinate("a3"), 2);
        expected.put(new Coordinate("b2"), 1);
        assertEquals(expected, res);
    }

    @Test
    public void test_makeOrder_BL() {
        HashMap<Coordinate, Integer> res;
        res = makeOrder(new Coordinate("A1"), "Battleship", 'L');
        HashMap<Coordinate, Integer> expected = new HashMap<>();
        expected.put(new Coordinate("b1"), 1);
        expected.put(new Coordinate("a2"), 4);
        expected.put(new Coordinate("b2"), 3);
        expected.put(new Coordinate("c2"), 2);
        assertEquals(expected, res);
    }

    @Test
    public void test_makeOrder_CD() {
        HashMap<Coordinate, Integer> res;
        res = makeOrder(new Coordinate("A1"), "Carrier", 'D');
        HashMap<Coordinate, Integer> expected = new HashMap<>();
        expected.put(new Coordinate("a1"), 7);
        expected.put(new Coordinate("b1"), 6);
        expected.put(new Coordinate("c1"), 5);
        expected.put(new Coordinate("b2"), 4);
        expected.put(new Coordinate("c2"), 3);
        expected.put(new Coordinate("d2"), 2);
        expected.put(new Coordinate("e2"), 1);
        assertEquals(expected, res);
    }

    @Test
    public void test_makeOrder_CL() {
        HashMap<Coordinate, Integer> res;
        res = makeOrder(new Coordinate("A1"), "Carrier", 'L');
        HashMap<Coordinate, Integer> expected = new HashMap<>();
        expected.put(new Coordinate("a3"), 5);
        expected.put(new Coordinate("a4"), 6);
        expected.put(new Coordinate("a5"), 7);
        expected.put(new Coordinate("b1"), 1);
        expected.put(new Coordinate("b2"), 2);
        expected.put(new Coordinate("b3"), 3);
        expected.put(new Coordinate("b4"), 4);
        assertEquals(expected, res);
    }

    @Test
    public void test_SpecialShapedShip() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('c', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 'c');
        SpecialShapedShip<Character> specialShapedShip = new SpecialShapedShip<Character>("Carrier", new Coordinate("A0"), 'U', myDisplayInfo, enemyDisplayInfo);
        HashMap<Coordinate, Boolean> expected = new HashMap<>();
        expected.put(new Coordinate("A0"), false);
        expected.put(new Coordinate("B0"), false);
        expected.put(new Coordinate("C0"), false);
        expected.put(new Coordinate("d0"), false);
        expected.put(new Coordinate("C1"), false);
        expected.put(new Coordinate("d1"), false);
        expected.put(new Coordinate("e1"), false);
        assertEquals(expected, specialShapedShip.myPieces);
    }

    @Test
    public void test_SpecialShapedShip1() {
        SpecialShapedShip<Character> specialShapedShip = new SpecialShapedShip<Character>("Carrier", new Coordinate("A0"), 'R', 'c', '*');
        HashMap<Coordinate, Boolean> expected = new HashMap<>();
        expected.put(new Coordinate("A1"), false);
        expected.put(new Coordinate("A2"), false);
        expected.put(new Coordinate("A3"), false);
        expected.put(new Coordinate("A4"), false);
        expected.put(new Coordinate("b0"), false);
        expected.put(new Coordinate("b1"), false);
        expected.put(new Coordinate("b2"), false);
        assertEquals(expected, specialShapedShip.myPieces);
        assertEquals("Carrier", specialShapedShip.getName());
    }
}