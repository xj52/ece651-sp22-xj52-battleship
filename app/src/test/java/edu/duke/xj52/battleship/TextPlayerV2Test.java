package edu.duke.xj52.battleship;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class TextPlayerV2Test {

    private TextPlayerV2 createTextPlayer(int w, int h, String inputData, OutputStream bytes, String name) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V2ShipFactory shipFactory = new V2ShipFactory();
        return new TextPlayerV2(name, board, input, output, shipFactory);
    }

    @Test
    public void test_playOneTurn() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayerV2 player1 = createTextPlayer(4, 3, "a0h\nf\nb2\nf\na1", bytes, "A");
        TextPlayerV2 player2 = createTextPlayer(4, 3, "B2V\nf\na0\nf\na1", bytes, "B");
        player1.doOnePlacement("Submarine", player1.shipCreationFns.get("Submarine"));
        player2.doOnePlacement("Submarine", player2.shipCreationFns.get("Submarine"));
        bytes.reset();
        player1.playOneTurn(player2.theBoard, player2.view, "B");
        String expected = "Player A's turn:\n" +
                "     Your ocean               Player B's ocean\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "A s|s| |  A                A  | | |  A\n" +
                "B  | | |  B                B  | | |  B\n" +
                "C  | | |  C                C  | | |  C\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "---------------------------------------------------------------------------\n" +
                "Possible actions for Player A:\n" +
                "\n" +
                "F Fire at a square\n" +
                "M Move a ship to another square (3 remaining)\n" +
                "S Sonar scan (3 remaining)\n" +
                "\n" +
                "Player A, what would you like to do?\n" +
                "---------------------------------------------------------------------------\n" +
                "\n" +
                "Where do you want to fire?\n" +
                "You hit a Submarine!\n";
        assertEquals(expected, bytes.toString());
        bytes.reset();
        player2.playOneTurn(player1.theBoard, player1.view, "A");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player2.playOneTurn(player1.theBoard, player1.view, "A");
        bytes.reset();
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        assertEquals("The winner is Player B!\n", bytes.toString());
    }

    @Test
    public void test_playOneTurn0() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayerV2 player1 = createTextPlayer(10, 20, "a0h\nb0h\nc0h\nd0h\ne0h\nf0u\nh0u\nj0u\nl0u\nl5u\n" +
                                                                        "m\na0\nabcd\nf\na0\n" +
                                                                        "m\nb0\nb0v\nf\na1\n" +
                                                                        "m\na0\na5h", bytes, "A");
        TextPlayerV2 player2 = createTextPlayer(10, 20, "a0h\nb0h\nc0h\nd0h\ne0h\nf0u\nh0u\nj0u\nl0u\nl5u\nf\na0\ns\naz\ns\na5", bytes, "B");
        player1.doPlacementPhase();
        player2.doPlacementPhase();
        bytes.reset();
        player2.playOneTurn(player1.theBoard, player1.view, "A");
        player2.playOneTurn(player1.theBoard, player1.view, "A");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        bytes.reset();
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        String expected = "Player A's turn:\n" +
                "     Your ocean                           Player B's ocean\n" +
                "  0|1|2|3|4|5|6|7|8|9                    0|1|2|3|4|5|6|7|8|9\n" +
                "A *|*| | | | | | | |  A                A s|s| | | | | | | |  A\n" +
                "B s|s| | | | | | | |  B                B  | | | | | | | | |  B\n" +
                "C d|d|d| | | | | | |  C                C  | | | | | | | | |  C\n" +
                "D d|d|d| | | | | | |  D                D  | | | | | | | | |  D\n" +
                "E d|d|d| | | | | | |  E                E  | | | | | | | | |  E\n" +
                "F  |b| | | | | | | |  F                F  | | | | | | | | |  F\n" +
                "G b|b|b| | | | | | |  G                G  | | | | | | | | |  G\n" +
                "H  |b| | | | | | | |  H                H  | | | | | | | | |  H\n" +
                "I b|b|b| | | | | | |  I                I  | | | | | | | | |  I\n" +
                "J  |b| | | | | | | |  J                J  | | | | | | | | |  J\n" +
                "K b|b|b| | | | | | |  K                K  | | | | | | | | |  K\n" +
                "L c| | | | |c| | | |  L                L  | | | | | | | | |  L\n" +
                "M c| | | | |c| | | |  M                M  | | | | | | | | |  M\n" +
                "N c|c| | | |c|c| | |  N                N  | | | | | | | | |  N\n" +
                "O c|c| | | |c|c| | |  O                O  | | | | | | | | |  O\n" +
                "P  |c| | | | |c| | |  P                P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q                Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R                R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S                S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T                T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9                    0|1|2|3|4|5|6|7|8|9\n" +
                "---------------------------------------------------------------------------\n" +
                "Possible actions for Player A:\n" +
                "\n" +
                "F Fire at a square\n" +
                "M Move a ship to another square (3 remaining)\n" +
                "S Sonar scan (3 remaining)\n" +
                "\n" +
                "Player A, what would you like to do?\n" +
                "---------------------------------------------------------------------------\n" +
                "\n" +
                "Which ship do you want to move?\n" +
                "Which placement do you want to move to?\n";
        assertEquals(expected, bytes.toString());
    }

    @Test
    public void test_playOneTurn1() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayerV2 player1 = createTextPlayer(10, 20, "a0h\nb0h\nc0h\nd0h\ne0h\nf0u\nh0u\nj0u\nl0u\nl5u\nm\na0\na5h\nm\nb0\nb5h\nm\nc0\nc5h\ns\na0\ns\nf1\ns\nl1\na0", bytes, "A");
        TextPlayerV2 player2 = createTextPlayer(10, 20, "a0h\nb0h\nc0h\nd0h\ne0h\nf0u\nh0u\nj0u\nl0u\nl5u\ns\nz9\na\ns\na6", bytes, "B");
        player1.doPlacementPhase();
        player2.doPlacementPhase();
        bytes.reset();
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        player2.playOneTurn(player1.theBoard, player1.view, "B");
        bytes.reset();
        player1.playOneTurn(player1.theBoard, player1.view, "B");
        String expected = "Player A's turn:\n" +
                "     Your ocean                           Player B's ocean\n" +
                "  0|1|2|3|4|5|6|7|8|9                    0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | |s|s| | |  A                A  | | | | | | | | |  A\n" +
                "B  | | | | |s|s| | |  B                B  | | | | | | | | |  B\n" +
                "C  | | | | |d|d|d| |  C                C  | | | | | | | | |  C\n" +
                "D d|d|d| | | | | | |  D                D  | | | | | | | | |  D\n" +
                "E d|d|d| | | | | | |  E                E  | | | | | | | | |  E\n" +
                "F  |b| | | | | | | |  F                F  | | | | | | | | |  F\n" +
                "G b|b|b| | | | | | |  G                G  | | | | | | | | |  G\n" +
                "H  |b| | | | | | | |  H                H  | | | | | | | | |  H\n" +
                "I b|b|b| | | | | | |  I                I  | | | | | | | | |  I\n" +
                "J  |b| | | | | | | |  J                J  | | | | | | | | |  J\n" +
                "K b|b|b| | | | | | |  K                K  | | | | | | | | |  K\n" +
                "L c| | | | |c| | | |  L                L  | | | | | | | | |  L\n" +
                "M c| | | | |c| | | |  M                M  | | | | | | | | |  M\n" +
                "N c|c| | | |c|c| | |  N                N  | | | | | | | | |  N\n" +
                "O c|c| | | |c|c| | |  O                O  | | | | | | | | |  O\n" +
                "P  |c| | | | |c| | |  P                P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q                Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R                R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S                S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T                T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9                    0|1|2|3|4|5|6|7|8|9\n" +
                "Where do you want to fire?\n" +
                "You missed!\n";
        assertEquals(expected, bytes.toString());
    }

    @Test
    public void test_checkCoordinate() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayerV2 player = createTextPlayer(4, 3, "a0h\nf\nb2\nf\na1", bytes, "A");
        assertFalse(player.checkCoordinate("a3f"));
        assertFalse(player.checkCoordinate("03"));
        assertFalse(player.checkCoordinate("Bh"));
    }

    @Test
    public void test_checkShipPlacement() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayerV2 player = createTextPlayer(4, 3, "a0h\nf\nb2\nf\na1", bytes, "A");
        V2ShipFactory v2ShipFactory = new V2ShipFactory();
        Ship<Character> battleship = v2ShipFactory.makeBattleship(new Placement("a0u"));
        Ship<Character> submarine = v2ShipFactory.makeSubmarine(new Placement("a0h"));
        assertFalse(player.checkShipPlacement("a000", battleship));
        assertFalse(player.checkShipPlacement("A0v", battleship));
        assertFalse(player.checkShipPlacement("A0r", submarine));
        assertFalse(player.checkShipPlacement("Z0V", submarine));
    }

    @Test
    public void test_isComputer() throws IOException {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20, 'X');
        BufferedReader input = new BufferedReader(new StringReader("c\nc\n"));
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        V2ShipFactory factory = new V2ShipFactory();
        TextPlayerV2 player1 = new TextPlayerV2("A", b1, input, output, factory);
        TextPlayerV2 player2 = new TextPlayerV2("B", b2, input, output, factory);
        player1.chooseComputerPhase("A");
        player2.chooseComputerPhase("B");
        player1.doPlacementPhase();
        player2.doPlacementPhase();
        int count = 0;
        while (count < 60) {
            player1.playOneTurn(player2.theBoard, player2.view, "B");
            player2.playOneTurn(player2.theBoard, player2.view, "A");
            count++;
        }
    }

    @Test
    public void test_doPlacementPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayerV2 player = createTextPlayer(4, 3, "a0h\nb0h\nc0h\nd0h\ne0h\nf0u\nh0u\nj0u\nl0u\nl5u", bytes, "B");
        player.doPlacementPhase();
        String expected = "  0|1|2|3\n" +
                "A  | | |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n" +
                "  0|1|2|3\n" +
                "\n" +
                "--------------------------------------------------------------------------------\n" +
                "Player B: you are going to place the following ships (Submarines and Destroyers\n" +
                "are rectangular, Battleships and Carriers are special-shaped). For each ship, type the \n" +
                "coordinate of the upper left side of the ship, followed by either H (for horizontal) or V \n" +
                "(for vertical) for Submarines and Destroyers or followed by U (for up) or D (for down) or L \n" +
                "(for left) or R (for right) for Battleships and Carriers.  For example M4H would place a ship \n" +
                "horizontally starting at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that has 4 squares\n" +
                "2 \"Carriers\" that has 7 squares\n" +
                "--------------------------------------------------------------------------------\n" +
                "\n" +
                "Player B where do you want to place a Submarine?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Submarine?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C  | | |  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Destroyer?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Destroyer?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Destroyer?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Battleship?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Battleship?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Battleship?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Carrier?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n" +
                "Player B where do you want to place a Carrier?\n" +
                "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n";
        assertEquals(expected, bytes.toString());
    }
}