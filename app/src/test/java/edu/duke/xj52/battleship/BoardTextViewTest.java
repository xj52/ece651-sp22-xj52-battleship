package edu.duke.xj52.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    private void nonEmptyBoardHelper(BoardTextView view, String expectedHeader, String expectedBody){
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_non_empty_4by3_enemy() {
        String expectedHeader = "  0|1|2|3\n";
        String expectedBody =
                        "A  | | |  A\n" +
                        "B  | | |  B\n" +
                        "C  | | |  C\n" ;
        Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
        Coordinate c1 = new Coordinate(0, 2);
        Coordinate c2 = new Coordinate(2, 1);
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c1, 's', '*', 'v')));
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c2, 's', '*', 'v')));
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayEnemyBoard());
    }

    @Test
    public void test_display_non_empty_4by3() {
        String expectedHeader = "  0|1|2|3\n";
        String expectedBody =
                        "A  | |s|  A\n" +
                        "B  | | |  B\n" +
                        "C  |s| |  C\n" ;
        Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
        Coordinate c1 = new Coordinate(0, 2);
        Coordinate c2 = new Coordinate(2, 1);
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c1, 's', '*', 'v')));
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c2, 's', '*', 'v')));
        //assertTrue(b1.tryAddShip(new BasicShip(c1)));
        //assertTrue(b1.tryAddShip(new BasicShip(c2)));
        BoardTextView view = new BoardTextView(b1);
        nonEmptyBoardHelper(view, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_non_empty_3by4() {
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                        "A  | |  A\n" +
                        "B  | |s B\n" +
                        "C  |s|  C\n" +
                        "D  | |  D\n" ;
        Board<Character> b1 = new BattleShipBoard<Character>(3, 4, 'X');
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(2, 1);
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c1, 's', '*', 'v')));
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c2, 's', '*', 'v')));
        //assertTrue(b1.tryAddShip(new BasicShip(c1)));
        //assertTrue(b1.tryAddShip(new BasicShip(c2)));
        BoardTextView view = new BoardTextView(b1);
        nonEmptyBoardHelper(view, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_2by2() {
        String expectedHeader = "  0|1\n";
        String expectedBody =
                        "A  |  A\n" +
                        "B  |  B\n";
        emptyBoardHelper(2, 2, expectedHeader, expectedBody);
        /*
        Board b1 = new BattleShipBoard(2, 2);
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader= "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                        expectedHeader+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());

         */
        /*
        String expected=
                        "  0|1\n"+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        "  0|1\n";
        assertEquals(expected, view.displayMyOwnBoard());
         */
    }

    @Test
    public void test_display_empty_3by2() {
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                        "A  | |  A\n" +
                        "B  | |  B\n";
        emptyBoardHelper(3, 2, expectedHeader, expectedBody);
        /*
        Board b1 = new BattleShipBoard(3, 2);
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1|2\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected =
                        expectedHeader +
                        "A  | |  A\n" +
                        "B  | |  B\n" +
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
         */
    }

    @Test
    public void test_display_empty_3by5() {
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                        "A  | |  A\n" +
                        "B  | |  B\n" +
                        "C  | |  C\n" +
                        "D  | |  D\n" +
                        "E  | |  E\n" ;
        emptyBoardHelper(3, 5, expectedHeader, expectedBody);
        /*
        Board b1 = new BattleShipBoard(3, 5);
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1|2\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected =
                        expectedHeader +
                        "A  | |  A\n" +
                        "B  | |  B\n" +
                        "C  | |  C\n" +
                        "D  | |  D\n" +
                        "E  | |  E\n" +
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());

         */
    }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27, 'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    @Test
    public void test_displayMyBoardWithEnemyNextToIt() {
        Board<Character> b_my = new BattleShipBoard<Character>(4, 2, 'X');
        Board<Character> b_enemy = new BattleShipBoard<Character>(4, 2, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Ship<Character> s_m1 = v1ShipFactory.makeSubmarine(new Placement("A0h"));
        Ship<Character> s_m2 = v1ShipFactory.makeDestroyer(new Placement("b1h"));
        b_my.tryAddShip(s_m1);
        b_my.tryAddShip(s_m2);
        Ship<Character> s_e1 = v1ShipFactory.makeSubmarine(new Placement("A0v"));
        Ship<Character> s_e2 = v1ShipFactory.makeDestroyer(new Placement("a1h"));
        b_enemy.tryAddShip(s_e1);
        b_enemy.tryAddShip(s_e2);
        BoardTextView view_m = new BoardTextView(b_my);
        BoardTextView view_e = new BoardTextView(b_enemy);
        String actual = view_m.displayMyBoardWithEnemyNextToIt(view_e, "Your ocean",  "Player B's ocean");
        String expected = "     Your ocean               Player B's ocean\n" +
                          "  0|1|2|3                    0|1|2|3\n" +
                          "A s|s| |  A                A  | | |  A\n"+
                          "B  |d|d|d B                B  | | |  B\n"+
                          "  0|1|2|3                    0|1|2|3\n";
        assertEquals(expected, actual);
    }
}