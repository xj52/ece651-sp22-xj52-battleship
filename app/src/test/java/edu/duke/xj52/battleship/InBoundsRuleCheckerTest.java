package edu.duke.xj52.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InBoundsRuleCheckerTest {

    @Test
    public void test_checkMyRule() {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, new InBoundsRuleChecker<>(null), 'X');
        V1ShipFactory s = new V1ShipFactory();
        Ship<Character> s1_2 = s.makeSubmarine(new Placement(new Coordinate(1, 2), 'H'));
        assertNull(b1.tryAddShip(s1_2));
        Ship<Character> b17_2 = s.makeBattleship(new Placement(new Coordinate(17, 2), 'v'));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", b1.tryAddShip(b17_2));
        Ship<Character> b_minus1_2 = s.makeBattleship(new Placement(new Coordinate(-1, 2), 'v'));
        assertEquals("That placement is invalid: the ship goes off the top of the board.", b1.tryAddShip(b_minus1_2));
        Ship<Character> b_10_minus1 = s.makeBattleship(new Placement(new Coordinate(10, -1), 'v'));
        assertEquals("That placement is invalid: the ship goes off the left of the board.", b1.tryAddShip(b_10_minus1));
        Ship<Character> b_5_25 = s.makeBattleship(new Placement(new Coordinate(5, 25), 'v'));
        assertEquals("That placement is invalid: the ship goes off the right of the board.", b1.tryAddShip(b_5_25));
    }
}