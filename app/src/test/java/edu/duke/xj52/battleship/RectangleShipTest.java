package edu.duke.xj52.battleship;

import static edu.duke.xj52.battleship.RectangleShip.makeCoords;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class RectangleShipTest {

    @Test
    public void test_makeCoords() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate(2, 1), 4, 1);
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(2, 1));
        expected.add(new Coordinate(2, 2));
        expected.add(new Coordinate(2, 3));
        expected.add(new Coordinate(2, 4));
        assertEquals(res, expected);
    }

    @Test
    public void test_makeCoords1() {
        HashSet<Coordinate> res;
        res = makeCoords(new Coordinate(3, 5), 1, 3);
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(3, 5));
        expected.add(new Coordinate(4, 5));
        expected.add(new Coordinate(5, 5));
        assertEquals(res, expected);
    }

    @Test
    public void test_RectangleShip() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 3, myDisplayInfo, enemyDisplayInfo, 'v');
        HashMap<Coordinate, Boolean> expected = new HashMap<>();
        expected.put(new Coordinate(3, 5), false);
        expected.put(new Coordinate(4, 5), false);
        expected.put(new Coordinate(5, 5), false);
        assertEquals(expected, rectangleShip.myPieces);
    }

    @Test
    public void test_recordHitAt() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 3, myDisplayInfo, enemyDisplayInfo, 'v');
        rectangleShip.recordHitAt(new Coordinate(3, 5));
        assertTrue(rectangleShip.myPieces.get(new Coordinate(3, 5)));
        assertEquals("submarine", rectangleShip.getName());
    }

    @Test
    public void test_wasHitAt() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 3, myDisplayInfo, enemyDisplayInfo, 'v');
        rectangleShip.recordHitAt(new Coordinate(4, 5));
        assertEquals(rectangleShip.myPieces.get(new Coordinate(4, 5)), rectangleShip.wasHitAt(new Coordinate(4, 5)));
    }

    @Test
    public void test_checkCoordinateInThisShip() {
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>(new Coordinate(3, 5), 's', '*', 'v');
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.recordHitAt(new Coordinate(5, 5)));//not on the ship
    }

    @Test
    public void test_isSunk_sunk() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 2, myDisplayInfo, enemyDisplayInfo, 'v');
        rectangleShip.recordHitAt(new Coordinate(3, 5));
        rectangleShip.recordHitAt(new Coordinate(4, 5));
        assertTrue(rectangleShip.isSunk());
    }

    @Test
    public void test_isSunk_not_sunk() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 2, myDisplayInfo, enemyDisplayInfo, 'v');
        rectangleShip.recordHitAt(new Coordinate(3, 5));
        assertFalse(rectangleShip.isSunk());
    }

    @Test
    public void test_getDisplayInfoAt() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 2, myDisplayInfo, enemyDisplayInfo, 'v');
        rectangleShip.recordHitAt(new Coordinate(3, 5));
        assertEquals('*', rectangleShip.getDisplayInfoAt(new Coordinate(3, 5), true));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(4, 5), true));
        assertEquals(null, rectangleShip.getDisplayInfoAt(new Coordinate(4, 5), false));
    }

    @Test
    public void test_getCoordinates() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(null, 's');
        RectangleShip<Character> rectangleShip = new RectangleShip<Character>("submarine", new Coordinate(3, 5), 1, 2, myDisplayInfo, enemyDisplayInfo, 'v');
        Set<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(3, 5));
        expected.add(new Coordinate(4, 5));
        assertEquals(expected, rectangleShip.getCoordinates());
    }

}
