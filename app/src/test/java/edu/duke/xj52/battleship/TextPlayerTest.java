package edu.duke.xj52.battleship;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class TextPlayerTest {

    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes, String name) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer(name, board, input, output, shipFactory);
    }

    @Test
    public void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes, "A");
        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');
        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }

    @Test
    public void test_readPlacement_empty() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "", bytes, "A");
        String prompt = "Please enter a location for a ship:";
        assertThrows(EOFException.class, () -> player.readPlacement(prompt));
    }

    @Test
    public void test_do_one_placement() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "A2V", bytes, "A");
        String prompt = "Player A where do you want to place a Destroyer?";
        String expectedHeader = "  0|1|2|3\n";
        String expected = expectedHeader +
                                "A  | |d|  A\n" +
                                "B  | |d|  B\n" +
                                "C  | |d|  C\n" +
                          expectedHeader;
        player.doOnePlacement("Destroyer", player.shipCreationFns.get("Destroyer"));
        assertEquals(prompt + "\n" + expected, bytes.toString());
    }

    @Test
    public void test_do_one_placement_computer() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "A2V", bytes, "A");
        player.isComputer = true;
        assertTrue(player.isComputer());
        player.doOnePlacement("Destroyer", player.shipCreationFns.get("Destroyer"));
        assertEquals("", bytes.toString());
    }

    @Test
    public void test_doPlacementPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "A0h\n" + "b0h\n" + "c0h\n" + "d0h\n" + "e0h\n" + "f0h\n" + "g0h\n" + "h0h\n" + "i0h\n" + "j0h\n", bytes, "A");
        player.doPlacementPhase();
        String expectedEmpBoard = "  0|1|2|3\n" +
                                        "A  | | |  A\n" +
                                        "B  | | |  B\n" +
                                        "C  | | |  C\n" +
                                        "  0|1|2|3\n";
        String expectedStart = "--------------------------------------------------------------------------------\n" +
                "Player A: you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" + "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n" +
                "--------------------------------------------------------------------------------\n";
        String promptS = "Player A where do you want to place a Submarine?";
        String promptD = "Player A where do you want to place a Destroyer?";
        String promptB = "Player A where do you want to place a Battleship?";
        String promptC = "Player A where do you want to place a Carrier?";
        String expectedBoard1 = "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n" +
                "  0|1|2|3\n";
        String expectedBoard2 = "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C  | | |  C\n" +
                "  0|1|2|3\n";
        String expectedBoard3 = "  0|1|2|3\n" +
                "A s|s| |  A\n" +
                "B s|s| |  B\n" +
                "C d|d|d|  C\n" +
                "  0|1|2|3\n";
        assertEquals(expectedEmpBoard + "\n" + expectedStart + "\n" + promptS + "\n" + expectedBoard1 +
                promptS + "\n" + expectedBoard2 + promptD + "\n" + expectedBoard3 +
                promptD + "\n" + expectedBoard3+ promptD + "\n" + expectedBoard3 + promptB + "\n" +
                expectedBoard3+ promptB + "\n" + expectedBoard3+ promptB + "\n" + expectedBoard3 + promptC +
                "\n" + expectedBoard3 + promptC + "\n" + expectedBoard3, bytes.toString());
    }

    @Test
    public void test_playOneTurn() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(4, 3, "B2V\nb2\nc2", bytes, "A");
        TextPlayer player2 = createTextPlayer(4, 3, "B2V", bytes, "B");
        player1.doOnePlacement("Submarine", player1.shipCreationFns.get("Submarine"));
        player2.doOnePlacement("Submarine", player2.shipCreationFns.get("Submarine"));
        bytes.reset();
        player1.playOneTurn(player2.theBoard, player2.view, "B");
        String expected1 = "Player A's turn:\n" +
                "     Your ocean               PlayerB's ocean\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "A  | | |  A                A  | | |  A\n" +
                "B  | |s|  B                B  | | |  B\n" +
                "C  | |s|  C                C  | | |  C\n" +
                "  0|1|2|3                    0|1|2|3" + "\n" +
                "You hit a Submarine!" + "\n";
        assertEquals(expected1, bytes.toString());
        bytes.reset();
        player1.playOneTurn(player2.theBoard, player2.view, "B");
        bytes.reset();
        player2.playOneTurn(player1.theBoard, player1.view, "A");
        String expected2 = "The winner is Player A!\n";
        assertEquals(expected2, bytes.toString());
    }

    @Test
    public void test_playOneTurn_none() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(4, 3, "B2V", bytes, "A");
        TextPlayer player2 = createTextPlayer(4, 3, "B2V", bytes, "B");
        player1.doOnePlacement("Submarine", player1.shipCreationFns.get("Submarine"));
        player2.doOnePlacement("Submarine", player2.shipCreationFns.get("Submarine"));
        bytes.reset();
        assertThrows(EOFException.class, () -> player1.playOneTurn(player2.theBoard, player2.view, "B"));

    }

    @Test
    public void test_playOneTurn_miss() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(4, 3, "B2V\nb1", bytes, "A");
        TextPlayer player2 = createTextPlayer(4, 3, "B2V", bytes, "B");
        player1.doOnePlacement("Submarine", player1.shipCreationFns.get("Submarine"));
        player2.doOnePlacement("Submarine", player2.shipCreationFns.get("Submarine"));
        bytes.reset();
        player1.playOneTurn(player2.theBoard, player2.view, "B");
        String expected = "Player A's turn:\n" +
                "     Your ocean               PlayerB's ocean\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "A  | | |  A                A  | | |  A\n" +
                "B  | |s|  B                B  | | |  B\n" +
                "C  | |s|  C                C  | | |  C\n" +
                "  0|1|2|3                    0|1|2|3" + "\n" +
                "You missed!" + "\n";
        assertEquals(expected, bytes.toString());
    }

    @Test
    public void test_randomPlacement() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "B2V", bytes, "S");
        String actual = player.randomPlacement("Submarine");
        String expected = "B0H";
        assertEquals(expected, actual);
    }

    @Test
    public void test_randomPlacement1() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "B2V", bytes, "B");
        String actual1 = player.randomPlacement("Battleship");
        String expected1 = "B0D";
        assertEquals(expected1, actual1);
        String actual2 = player.randomPlacement("Carrier");
        String expected2 = "B0R";
        assertEquals(expected2, actual2);
        String actual3 = player.randomPlacement("Carrier");
        String expected3 = "B2R";
        assertEquals(expected3, actual3);
        String actual4 = player.randomPlacement("Carrier");
        String expected4 = "B0D";
        assertEquals(expected4, actual4);
        String actual5 = player.randomPlacement("Carrier");
        String expected5 = "A0L";
        assertEquals(expected5, actual5);
        String actual6 = player.randomPlacement("Carrier");
        String expected6 = "A3D";
        assertEquals(expected6, actual6);
        String actual7 = player.randomPlacement("Carrier");
        String expected7 = "C1L";
        assertEquals(expected7, actual7);
    }

    @Test
    public void test_choseComputerPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(4, 3, "c", bytes, "B");
        player1.chooseComputerPhase("A");
        assertTrue(player1.isComputer());
        TextPlayer player2 = createTextPlayer(4, 3, "h", bytes, "B");
        player2.chooseComputerPhase("A");
        assertFalse(player2.isComputer());
        TextPlayer player3 = createTextPlayer(4, 3, "z", bytes, "B");
        assertThrows(IllegalArgumentException.class, () -> player3.chooseComputerPhase("A"));
        TextPlayer player4 = createTextPlayer(4, 3, "", bytes, "B");
        assertThrows(EOFException.class, () -> player4.chooseComputerPhase("A"));
    }

    /*
    @Test
    public void test_doPlacementPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "A2V", bytes);
        player.doPlacementPhase();
        String expectedHeader = "  0|1|2|3\n";
        String expectedEmpty = expectedHeader +
                "A  | | |  A\n" +
                "B  | | |  B\n" +
                "C  | | |  C\n" +
                expectedHeader;
        String begin = "--------------------------------------------------------------------------------\n" +
                "Player A: you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" + "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n" +
                "--------------------------------------------------------------------------------\n";
        String prompt = "Player A where do you want to place a Destroyer?";
        String expectedRes = expectedHeader +
                "A  | |d|  A\n" +
                "B  | |d|  B\n" +
                "C  | |d|  C\n" +
                expectedHeader;
        assertEquals(expectedEmpty + "\n"+ begin + "\n" + prompt + "\n" + expectedRes + "\n", bytes.toString());
    }
     */
}