package edu.duke.xj52.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {

    @Test
    public void test_SimpleShipDisplayInfo() {
        SimpleShipDisplayInfo<Character> test = new SimpleShipDisplayInfo<>('a', 's');
        assertEquals('a', test.myData);
        assertEquals('s', test.onHit);
    }

    @Test
    public void test_getInfo() {
        SimpleShipDisplayInfo<Character> test = new SimpleShipDisplayInfo<>('m', 's');
        assertEquals('m', test.getInfo(new Coordinate(2, 1), false));
        assertEquals('s', test.getInfo(new Coordinate(3, 5), true));
    }
}
