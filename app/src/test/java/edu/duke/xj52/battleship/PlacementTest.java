package edu.duke.xj52.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {

    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(1, 3);
        Coordinate c4 = new Coordinate(3, 2);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c3, 'v');
        Placement p4 = new Placement(c4, 'h');
        assertEquals(p1, p1);   //equals should be reflexsive
        assertEquals(p1, p2);   //different objects bu same contents
        assertNotEquals(p1, p3);  //different contents
        assertNotEquals(p1, p4);
        assertNotEquals(p3, p4);
        assertNotEquals(p1, "(1, 2, v)"); //different types
    }

    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(0, 3);
        Coordinate c4 = new Coordinate(2, 1);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c3, 'v');
        Placement p4 = new Placement(c2, 'h');
        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p3.hashCode(), p4.hashCode());
    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3v");
        Coordinate c1 = new Coordinate(1, 3);
        assertEquals(c1, p1.getWhere());
        assertEquals('V', p1.getOrientation());
        Placement p2 = new Placement("D5H");
        assertEquals(3, p2.where.getRow());
        assertEquals(5, p2.where.getColumn());
        assertEquals('H', p2.getOrientation());
        Placement p3 = new Placement("A9V");
        assertEquals(0, p3.where.getRow());
        assertEquals(9, p3.where.getColumn());
        assertEquals('V', p3.getOrientation());
        Placement p4 = new Placement("Z0h");
        assertEquals(25, p4.where.getRow());
        assertEquals(0, p4.where.getColumn());
        assertEquals('H', p4.getOrientation());
    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("00V"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAv"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("Z0["));
        assertThrows(IllegalArgumentException.class, () -> new Placement("B8/"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A2:"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("C5@"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A12H"));
    }
}
