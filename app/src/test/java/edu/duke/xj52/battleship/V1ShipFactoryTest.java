package edu.duke.xj52.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c : expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
        }
    }

    @Test
    public void test_makeSubmarine() {
        V1ShipFactory f = new V1ShipFactory();
        Ship<Character> ship = f.makeSubmarine(new Placement(new Coordinate(2, 4), 'H'));
        checkShip(ship, "Submarine", 's', new Coordinate(2, 4), new Coordinate(2, 5));
    }

    @Test
    public void test_makeSubmarine_ori_fault() {
        V1ShipFactory f = new V1ShipFactory();
        assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(new Placement(new Coordinate(2, 4), 'z')));
    }

    @Test
    public void test_makeBattleship() {
        V1ShipFactory f = new V1ShipFactory();
        Ship<Character> ship = f.makeBattleship(new Placement(new Coordinate(1, 1), 'v'));
        checkShip(ship, "Battleship", 'b', new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate(3, 1), new Coordinate(4, 1));
    }

    @Test
    public void test_makeCarrier() {
        V1ShipFactory f = new V1ShipFactory();
        Ship<Character> ship = f.makeCarrier(new Placement(new Coordinate(2, 1), 'H'));
        checkShip(ship, "Carrier", 'c', new Coordinate(2, 1), new Coordinate(2, 2), new Coordinate(2, 3), new Coordinate(2, 4), new Coordinate(2, 5), new Coordinate(2, 6));
    }

    @Test
    public void test_makeDestroyer() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        V1ShipFactory f = new V1ShipFactory();
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    }
}
