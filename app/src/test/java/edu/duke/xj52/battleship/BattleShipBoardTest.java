package edu.duke.xj52.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
        /*
        BattleShipBoard b1 = new BattleShipBoard(10, 20);
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
         */
    }

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(-8, 20, 'X'));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected, boolean isSelf){
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                Coordinate c = new Coordinate(i, j);
                if (isSelf) {
                    assertEquals(expected[i][j], b.whatIsAtForSelf(c));
                }
                else {
                    assertEquals(expected[i][j], b.whatIsAtForEnemy(c));
                }
            }
        }
    }
    @Test
    public void test_whatIsAt_tryAddShip() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Character[][] expected = new Character[20][10];
        //no ship -- all coordinates should return null
        checkWhatIsAtBoard(b1, expected, true);

        //add ships -- right coordinate should return 's' and tryAddShip return true
        expected[0][5] = 's';
        expected[15][8] = 's';
        expected[2][9] = 's';
        expected[9][2] = 's';
        Coordinate c1 = new Coordinate(0, 5);
        Coordinate c2 = new Coordinate(15, 8);
        Coordinate c3 = new Coordinate(2, 9);
        Coordinate c4 = new Coordinate(9, 2);
        //tryAddShip should return null if true
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c1, 's', '*', 'v')));
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c2, 's', '*', 'v')));
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c3, 's', '*', 'v')));
        assertNull(b1.tryAddShip(new RectangleShip<Character>(c4, 's', '*', 'v')));
        //assertTrue(b1.tryAddShip(new BasicShip(c1)));
        //assertTrue(b1.tryAddShip(new BasicShip(c2)));
        //assertTrue(b1.tryAddShip(new BasicShip(c3)));
        //assertTrue(b1.tryAddShip(new BasicShip(c4)));
        checkWhatIsAtBoard(b1, expected, true);
    }

    @Test
    public void test_whatIsAtForEnemy_getMyShips_belongsToShip_deleteShip_readdShip() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Character[][] expected = new Character[20][10];
        //no ship -- all coordinates should return null
        checkWhatIsAtBoard(b1, expected, false);
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Ship<Character> b0_5 = v1ShipFactory.makeBattleship(new Placement(new Coordinate(0, 5), 'v'));
        b1.tryAddShip(b0_5);
        Ship<Character> b0_1 = v1ShipFactory.makeBattleship(new Placement(new Coordinate(0, 1), 'v'));
        b1.tryAddShip(b0_1);
        //getMyShips
        ArrayList<Ship<Character>> actualList = b1.getMyShips();
        ArrayList<Ship<Character>> ships = new ArrayList<Ship<Character>>();
        ships.add(b0_5);
        ships.add(b0_1);
        assertEquals(ships, actualList);
        //belongs to ship
        Ship<Character> s1 = b1.belongsToShip(new Coordinate(1, 5));
        Ship<Character> s2 = b1.belongsToShip(new Coordinate(1, 1));
        assertEquals(b1.myShips.get(0), s1);
        assertEquals(b1.myShips.get(1), s2);
        assertNull(b1.belongsToShip(new Coordinate(0, 6)));
        //delete ship
        b1.deleteShip(b0_1);
        assertFalse(b1.myShips.contains(b0_1));
        //readd ship
        b1.readdShip(b0_1);
        assertTrue(b1.myShips.contains(b0_1));
        b1.fireAt(new Coordinate(0, 5));
        expected[0][5] = 'b';
        checkWhatIsAtBoard(b1, expected, false);
        b1.fireAt(new Coordinate(0, 6));
        expected[0][6] = 'X';
        checkWhatIsAtBoard(b1, expected, false);
    }

    @Test
    public void test_fireAt() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Ship<Character> b0_5 = v1ShipFactory.makeBattleship(new Placement(new Coordinate(0, 5), 'v'));
        b1.tryAddShip(b0_5);
        b1.fireAt(new Coordinate(0, 5));
        assertFalse(b0_5.isSunk());
        b1.fireAt(new Coordinate(1, 5));
        assertFalse(b0_5.isSunk());
        b1.fireAt(new Coordinate(2, 5));
        assertFalse(b0_5.isSunk());
        Ship<Character> s = b1.fireAt(new Coordinate(3, 5));
        assertTrue(b0_5.isSunk());
        assertSame(b0_5, s);
        b1.fireAt(new Coordinate(4, 5)); //missing
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(4, 5));
        assertEquals(expected, b1.enemyMisses);
    }

    @Test
    public void test_checkLose() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Ship<Character> b0_5 = v1ShipFactory.makeBattleship(new Placement(new Coordinate(0, 5), 'v'));
        Ship<Character> b0_1 = v1ShipFactory.makeBattleship(new Placement(new Coordinate(0, 1), 'v'));
        b1.tryAddShip(b0_5);
        b1.tryAddShip(b0_1);
        b1.fireAt(new Coordinate(0, 5));
        b1.fireAt(new Coordinate(1, 5));
        b1.fireAt(new Coordinate(2, 5));
        b1.fireAt(new Coordinate(3, 5));
        assertTrue(b0_5.isSunk());
        assertFalse(b1.CheckLose());
        b1.fireAt(new Coordinate(0, 1));
        b1.fireAt(new Coordinate(1, 1));
        b1.fireAt(new Coordinate(2, 1));
        b1.fireAt(new Coordinate(3, 1));
        assertTrue(b0_1.isSunk());
        assertTrue(b1.CheckLose());
    }
}
