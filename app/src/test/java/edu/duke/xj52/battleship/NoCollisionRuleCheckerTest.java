package edu.duke.xj52.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NoCollisionRuleCheckerTest {

    @Test
    public void test_checkMyRule() {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, new NoCollisionRuleChecker<>(null), 'X');
        V1ShipFactory s = new V1ShipFactory();
        Ship<Character> s1_2 = s.makeSubmarine(new Placement(new Coordinate(1, 2), 'H'));
        assertNull(b1.tryAddShip(s1_2));
        Ship<Character> c1_2 = s.makeCarrier(new Placement(new Coordinate(1, 2), 'h'));
        assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(c1_2));
    }

    @Test
    public void test_InBounds_and_NoCollision() {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, new NoCollisionRuleChecker<>(new InBoundsRuleChecker<>(null)), 'X');
        V1ShipFactory s = new V1ShipFactory();
        Ship<Character> s1_2 = s.makeSubmarine(new Placement(new Coordinate(1, 2), 'H'));
        assertNull(b1.tryAddShip(s1_2));
        Ship<Character> b17_2 = s.makeBattleship(new Placement(new Coordinate(17, 2), 'v'));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", b1.tryAddShip(b17_2));
        Ship<Character> c1_2 = s.makeCarrier(new Placement(new Coordinate(1, 2), 'h'));
        assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(c1_2));
    }
}