  0|1|2|3|4|5|6|7|8|9
A  | | | | | | | | |  A
B  | | | | | | | | |  B
C  | | | | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9

--------------------------------------------------------------------------------
Player A: you are going to place the following ships (which are all
rectangular). For each ship, type the coordinate of the upper left
side of the ship, followed by either H (for horizontal) or V (for
vertical).  For example M4H would place a ship horizontally starting
at M4 and going to the right.  You have

2 "Submarines" ships that are 1x2
3 "Destroyers" that are 1x3
3 "Battleships" that are 1x4
2 "Carriers" that are 1x6
--------------------------------------------------------------------------------

Player A where do you want to place a Submarine?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B  | | | | | | | | |  B
C  | | | | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Submarine?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Destroyer?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Destroyer?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Destroyer?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Battleship?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F b|b|b|b| | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Battleship?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F b|b|b|b| | | | | |  F
G b|b|b|b| | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Battleship?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F b|b|b|b| | | | | |  F
G b|b|b|b| | | | | |  G
H b|b|b|b| | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Carrier?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F b|b|b|b| | | | | |  F
G b|b|b|b| | | | | |  G
H b|b|b|b| | | | | |  H
I c|c|c|c|c|c| | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player A where do you want to place a Carrier?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C d|d|d| | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F b|b|b|b| | | | | |  F
G b|b|b|b| | | | | |  G
H b|b|b|b| | | | | |  H
I c|c|c|c|c|c| | | |  I
J c|c|c|c|c|c| | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
  0|1|2|3|4|5|6|7|8|9
A  | | | | | | | | |  A
B  | | | | | | | | |  B
C  | | | | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9

--------------------------------------------------------------------------------
Player B: you are going to place the following ships (which are all
rectangular). For each ship, type the coordinate of the upper left
side of the ship, followed by either H (for horizontal) or V (for
vertical).  For example M4H would place a ship horizontally starting
at M4 and going to the right.  You have

2 "Submarines" ships that are 1x2
3 "Destroyers" that are 1x3
3 "Battleships" that are 1x4
2 "Carriers" that are 1x6
--------------------------------------------------------------------------------

Player B where do you want to place a Submarine?
  0|1|2|3|4|5|6|7|8|9
A s| | | | | | | | |  A
B s| | | | | | | | |  B
C  | | | | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Submarine?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D  | | | | | | | | |  D
E  | | | | | | | | |  E
F  | | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Destroyer?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d| | | | | | | | |  D
E d| | | | | | | | |  E
F d| | | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Destroyer?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d| | | | | | | |  D
E d|d| | | | | | | |  E
F d|d| | | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Destroyer?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F d|d|d| | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J  | | | | | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Battleship?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F d|d|d| | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I  | | | | | | | | |  I
J b|b|b|b| | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Battleship?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F d|d|d| | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | | |  H
I b|b|b|b| | | | | |  I
J b|b|b|b| | | | | |  J
K  | | | | | | | | |  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Battleship?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F d|d|d| | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | |b|  H
I b|b|b|b| | | | |b|  I
J b|b|b|b| | | | |b|  J
K  | | | | | | | |b|  K
L  | | | | | | | | |  L
M  | | | | | | | | |  M
N  | | | | | | | | |  N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Carrier?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F d|d|d| | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | |b|  H
I b|b|b|b| | | | |b|c I
J b|b|b|b| | | | |b|c J
K  | | | | | | | |b|c K
L  | | | | | | | | |c L
M  | | | | | | | | |c M
N  | | | | | | | | |c N
O  | | | | | | | | |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
Player B where do you want to place a Carrier?
  0|1|2|3|4|5|6|7|8|9
A s|s| | | | | | | |  A
B s|s| | | | | | | |  B
C  | | | | | | | | |  C
D d|d|d| | | | | | |  D
E d|d|d| | | | | | |  E
F d|d|d| | | | | | |  F
G  | | | | | | | | |  G
H  | | | | | | | |b|  H
I b|b|b|b| | | | |b|c I
J b|b|b|b| | | |c|b|c J
K  | | | | | | |c|b|c K
L  | | | | | | |c| |c L
M  | | | | | | |c| |c M
N  | | | | | | |c| |c N
O  | | | | | | |c| |  O
P  | | | | | | | | |  P
Q  | | | | | | | | |  Q
R  | | | | | | | | |  R
S  | | | | | | | | |  S
T  | | | | | | | | |  T
  0|1|2|3|4|5|6|7|8|9
