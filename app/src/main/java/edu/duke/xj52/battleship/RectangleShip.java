package edu.duke.xj52.battleship;

import java.util.HashMap;
import java.util.HashSet;

/**
 * This class constructs a rectangle ship which
 * inherits from class BasicShip
 */
public class RectangleShip<T> extends BasicShip<T> {

    final String name;

    /**
     * constructs a hash set to include all the coordinates of a rectangle ship.
     * @param upperLeft is the upper left coordinate of the rectangle ship.
     * @param width is the width of the rectangle ship.
     * @param height is the height of the rectangle ship.
     * @return a hash set containing all the coordinates of the rectangle ship.
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> coordinates = new HashSet<>();
        for (int i = upperLeft.getColumn(); i < width + upperLeft.getColumn(); i++) {
            for (int j = upperLeft.getRow(); j < height + upperLeft.getRow(); j++) {
                coordinates.add(new Coordinate(j, i));
            }
        }
        return coordinates;
    }

    /**
     *
     * @param upperLeft is the upper left coordinate of the rectangle ship.
     * @param width is the width of the rectangle ship.
     * @param height is the height of the rectangle ship.
     * @param orientation is the orientation of the rectangle ship.
     * @return a hash map containing all the coordinates of the rectangle ship and the corresponding numbers.
     */
    static HashMap<Coordinate, Integer> makeOrder(Coordinate upperLeft, int width, int height, char orientation) {
        HashMap<Coordinate, Integer> order = new HashMap<>();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        int count = 1;
        if (orientation == 'V' && width == 1) {
            for (int i = row; i < row + height; i++) {
                order.put(new Coordinate(i, col), count);
                count++;
            }
        }
        if (orientation == 'H' && height == 1) {
            for (int j = col; j < width + col; j++) {
                order.put(new Coordinate(row, j), count);
                count++;
            }
        }
        return order;
    }

    /**
     * constructs a rectangle ship.
     * @param name is the name of the rectangle ship.
     * @param upperLeft is the upper left coordinate of the rectangle ship.
     * @param width is the width of the rectangle ship.
     * @param height is the height of the rectangle ship.
     * @param myDisplayInfo is the information of my rectangle ship.
     * @param enemyDisplayInfo is the information of enemy's rectangle ship.
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, char orientation) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo, makeOrder(upperLeft, width, height, orientation), orientation);
        this.name = name;
    }

    /**
     * constructs a rectangle ship.
     * @param name is the name of the rectangle ship.
     * @param upperLeft is the upper left coordinate of the rectangle ship.
     * @param width is the width of the rectangle ship.
     * @param height is the height of the rectangle ship.
     * @param data is the data of the ship.
     * @param onHit is the on hit information of the ship.
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit, char orientation) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data), orientation);
    }

    /**
     * constructs a rectangle ship.
     * @param upperLeft is the upper left coordinate of the rectangle ship.
     * @param data is the data of the ship.
     * @param onHit is the on hit information of the ship.
     */
    public RectangleShip(Coordinate upperLeft, T data, T onHit, char orientation) {
        this("testship", upperLeft, 1, 1, data, onHit, orientation);
    }

    public String getName() {
        return name;
    }
}
