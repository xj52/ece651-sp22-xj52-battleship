package edu.duke.xj52.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;

public class TextPlayerV2 extends TextPlayer {

    private int mTime;
    private int sTime;

    /**
     * constructs an object of TextPlayerV2
     * @param name is the name of the player.
     * @param theBoard is the board of the player.
     * @param inputReader is the input reader.
     * @param out is the output stream.
     * @param shipFactory is the ship factory.
     */
    public TextPlayerV2(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        super(name, theBoard, inputReader, out, shipFactory);
        this.mTime = 3;
        this.sTime = 3;
    }

    /**
     * Displays the prompt and related information for the players.
     * @throws IOException
     */
    @Override
    public void doPlacementPhase() throws IOException {
        if (!isComputer) {
            out.println(view.displayMyOwnBoard());
            String start = "--------------------------------------------------------------------------------\n" +
                    "Player " + name + ": you are going to place the following ships (Submarines and Destroyers\n" +
                    "are rectangular, Battleships and Carriers are special-shaped). For each ship, type the \n" +
                    "coordinate of the upper left side of the ship, followed by either H (for horizontal) or V \n" +
                    "(for vertical) for Submarines and Destroyers or followed by U (for up) or D (for down) or L \n" +
                    "(for left) or R (for right) for Battleships and Carriers.  For example M4H would place a ship \n" +
                    "horizontally starting at M4 and going to the right.  You have\n" +
                    "\n" +
                    "2 \"Submarines\" ships that are 1x2\n" +
                    "3 \"Destroyers\" that are 1x3\n" +
                    "3 \"Battleships\" that has 4 squares\n" +
                    "2 \"Carriers\" that has 7 squares\n" +
                    "--------------------------------------------------------------------------------\n";
            out.println(start);
        }
        for (String s: shipsToPlace) {
            this.doOnePlacement(s, shipCreationFns.get(s));
        }
    }

    /**
     * plays one turn for each player.
     * @param enemyBoard is the board of enemy.
     * @param enemyView is the board text view of enemy.
     * @param enemyName is enemy's name.
     * @return false if the player is the winner, otherwise true.
     * @throws IOException
     */
    @Override
    public boolean playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        if (theBoard.CheckLose()) {
            out.println("The winner is Player " + enemyName + "!");
            return false;
        }
        if (!isComputer()) {
            out.println("Player " + name + "'s turn:");
            String enemyHeader = "Player " + enemyName + "'s ocean";
            out.print(view.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", enemyHeader));
        }
        if (mTime > 0 || sTime > 0) {
            playOneTurnV2(enemyBoard, enemyView, enemyName);
        }
        else {
            fire(enemyBoard);
        }
        return true;
    }

    /**
     * plays one turn for each player in version 2.
     * @param enemyBoard is the board of enemy.
     * @param enemyView is the board text view of enemy.
     * @param enemyName is enemy's name.
     * @throws IOException
     */
    public void playOneTurnV2(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        String choseAction;
        if (!isComputer()) {
            String action = "---------------------------------------------------------------------------\n" +
                    "Possible actions for Player " + name + ":\n" + "\n" +
                    "F Fire at a square\n" +
                    "M Move a ship to another square (" + mTime + " remaining)\n" +
                    "S Sonar scan (" + sTime + " remaining)\n" + "\n" +
                    "Player " + name + ", what would you like to do?\n" +
                    "---------------------------------------------------------------------------\n";
            out.println(action);
            choseAction = readLineAndCheckNull(); //choose action
        }
        else { //computer
            int m = random.nextInt(3);
            if (m == 0) choseAction = "F";
            else if (mTime > 0 && m == 1) choseAction = "M";
            else if (sTime > 0 && m == 2) choseAction = "S";
            else choseAction = "F";
        }
        if (mTime > 0 && choseAction.equals("M")) {
            move(enemyBoard, enemyView, enemyName);
        }
        else if (sTime > 0 && choseAction.equals("S")) {
            sonar(enemyBoard, enemyView, enemyName);
        }
        else if (choseAction.equals("F")) {
            fire(enemyBoard);
        }
        else {
            playOneTurnV2(enemyBoard, enemyView, enemyName);
        }
    }

    /**
     * implements choice fire.
     * @param enemyBoard is the board of enemy.
     * @throws IOException
     */
    public void fire(Board<Character> enemyBoard) throws IOException {
        Ship<Character> s;
        String randomCoor = null;
        if (!isComputer()) {
            out.println("Where do you want to fire?");
            String location = readLineAndCheckNull();
            s = enemyBoard.fireAt(new Coordinate(location));
        }
        else {
            randomCoor = randomCoordinate();
            s = enemyBoard.fireAt(new Coordinate(randomCoor));
        }
        if (s == null) {
            if (!isComputer()) {
                out.println("You missed!");
            }
            else {
                out.println("Player " + name + " missed!");
            }
        }
        else {
            if (!isComputer()) {
                out.println("You hit a " + s.getName() + "!");
            }
            else {
                out.println("Player " + name + " hit your " + s.getName() + " at " + randomCoor + "!");
            }
        }
    }

    /**
     * checks if the given string is a valid coordinate.
     * @param where is a string representing a coordinate.
     * @return true if it is a valid coordinate, otherwise false.
     */
    public boolean checkCoordinate(String where) {
        if(where.length() != 2) {
            return false;
        }
        char rowLetter = where.charAt(0);
        char columnLetter = where.charAt(1);
        if (rowLetter < 'A' || rowLetter > 'Z') {
            return false;
        }
        if (columnLetter < '0' || columnLetter > '9') {
            return false;
        }
        return true;
    }

    /**
     * checks if the given string belongs to a ship.
     * @param location is a string representing a coordinate.
     * @return a ship if the given location belongs to a ship on the board, else null.
     */
    public Ship<Character> checkShipCoordinate(String location) {
        if (!checkCoordinate(location)) return null;
        Coordinate c = new Coordinate(location);
        return theBoard.belongsToShip(c);
    }

    /**
     * checks if the given string is a valid placement for the given ship to place.
     * @param placement is a string representing a placement.
     * @param s is the ship to be placed.
     * @return true if the given string is a valid placement, otherwise false.
     */
    public boolean checkShipPlacement(String placement, Ship<Character> s) {
        if(placement.length() != 3) {
            return false;
        }
        String coor = placement.substring(0, 2);
        if (!checkCoordinate(coor)) return false;
        String ori = placement.substring(2);
        char oriLetter = ori.charAt(0);
        if (s.getName().equals("Submarine") || s.getName().equals("Destroyer")) {
            if (oriLetter != 'H' && oriLetter != 'V') {
                return false;
            }
        }
        if (s.getName().equals("Battleship") || s.getName().equals("Carrier")) {
            if (oriLetter != 'U' && oriLetter != 'D' && oriLetter != 'L' && oriLetter != 'R') {
                return false;
            }
        }
        Placement p = new Placement(placement);
        Ship<Character> newShip = shipCreationFns.get(s.getName()).apply(p);
        String checkRes = theBoard.getPlacementChecker().checkPlacement(newShip, theBoard);
        if (checkRes != null) {
            return false;
        }
        return true;
    }

    /**
     * implements choice move.
     * @param enemyBoard is the board of enemy.
     * @param enemyView is the board text view of enemy.
     * @param enemyName is enemy's name.
     * @throws IOException
     */
    public void move(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        Ship<Character> s;
        if (!isComputer()) {
            out.println("Which ship do you want to move?");
            String location = readLineAndCheckNull();
            s = checkShipCoordinate(location);
        }
        else {
            s = checkShipCoordinate(randomCoordinate());
        }
        if (s == null) {
            playOneTurnV2(enemyBoard, enemyView, enemyName);
        }
        else { //have found the old ship, move to a new place
            String p;
            if (!isComputer()) {
                out.println("Which placement do you want to move to?");
                p = readLineAndCheckNull();
            }
            else { //computer
                p = randomPlacement(s.getName());
            }
            theBoard.deleteShip(s);
            if (!checkShipPlacement(p, s)) {
                theBoard.readdShip(s);
                playOneTurnV2(enemyBoard, enemyView, enemyName);
                return;
            }
            // the given placement is correct
            Placement placement = new Placement(p);
            Ship<Character> newShip = shipCreationFns.get(s.getName()).apply(placement);
            theBoard.tryAddShip(newShip);
            for (Coordinate c : s.getMyPieces().keySet()) { //find all hit pieces in the old ship
                if (s.getMyPieces().get(c)) {
                    int ind = s.getOrder().get(c);//find index of the old ship
                    for (Coordinate newC : newShip.getMyPieces().keySet()) { //fire new ship with the corresponding index
                        if (newShip.getOrder().get(newC) == ind) {
                            newShip.recordHitAt(newC);
                        }
                    }
                }
            }
            mTime--;
            if (isComputer()) {
                out.println("Player " + name + " used a special action");
            }
        }
    }

    /**
     * checks if a coordinate is on the board.
     * @param c is the given coordinate.
     * @return true if the coordinate is on the board, otherwise false.
     */
    public boolean checkInBound(Coordinate c) {
        if (c.getRow() < 0 || c.getRow() >= theBoard.getHeight() || c.getColumn() < 0 || c.getColumn() >= theBoard.getWidth()) {
            return false;
        }
        return true;
    }

    /**
     * helps makeSonarSet function to generate a line of sonar points.
     * @param start is the starting index of for loop
     * @param length is the length of the line.
     * @param row is the row index of the line.
     * @param coordinates is a hash set containing existing sonar points.
     * @return a hash set of sonar points.
     */
    public HashSet<Coordinate> makeSonarSetHelper(int start, int length, int row, HashSet<Coordinate> coordinates) {
        if (row < 0 || row >= theBoard.getHeight()) {
            return coordinates;
        }
        for (int i = start; i < start + length; i++) {
            if (i >= 0 && i < theBoard.getWidth()) {
                coordinates.add(new Coordinate(row, i));
            }
        }
        return coordinates;
    }

    /**
     * makes a hash set for all the sonar points.
     * @param c is a coordinate which is the center of the sonar.
     * @return a hash set of all sonar points.
     */
    public HashSet<Coordinate> makeSonarSet(Coordinate c) {
        HashSet<Coordinate> coordinates = new HashSet<>();
        coordinates = makeSonarSetHelper(c.getColumn(), 1, c.getRow() - 3, coordinates);
        coordinates = makeSonarSetHelper(c.getColumn() - 1, 3, c.getRow() - 2, coordinates);
        coordinates = makeSonarSetHelper(c.getColumn() - 2, 5, c.getRow() - 1, coordinates);
        coordinates = makeSonarSetHelper(c.getColumn() - 3, 7, c.getRow(), coordinates);
        coordinates = makeSonarSetHelper(c.getColumn() - 2, 5, c.getRow() + 1, coordinates);
        coordinates = makeSonarSetHelper(c.getColumn() - 1, 3, c.getRow() + 2, coordinates);
        coordinates = makeSonarSetHelper(c.getColumn(), 1, c.getRow() + 3, coordinates);
        return coordinates;
    }

    /**
     * implements choice sonar.
     * @param enemyBoard is the board of enemy.
     * @param enemyView is the board text view of enemy.
     * @param enemyName is enemy's name.
     * @throws IOException
     */
    public void sonar(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        String c;
        if (!isComputer()) {
            out.println("Where do you want to set your sonar?");
            c = readLineAndCheckNull();
            if (!checkCoordinate(c)) {
                playOneTurnV2(enemyBoard, enemyView, enemyName);
                return;
            }
        }
        else {
            c = randomCoordinate();
        }
        Coordinate coordinate = new Coordinate(c);
        int submarines = 0;
        int destroyers = 0;
        int battleships = 0;
        int carriers = 0;
        if (checkInBound(coordinate)) { //given coordinate is on the board
            HashSet<Coordinate> sonarElements = makeSonarSet(coordinate);
            for (Coordinate sonarElement : sonarElements) { //start to scan
                for (Ship<Character> s: enemyBoard.getMyShips()) {
                    if (s.occupiesCoordinates(sonarElement)) {
                        if (s.getName().equals("Submarine")) submarines++;
                        else if (s.getName().equals("Destroyer")) destroyers++;
                        else if (s.getName().equals("Battleship")) battleships++;
                        else if (s.getName().equals("Carrier")) carriers++;
                    }
                }
            }
            if (!isComputer()) {
                out.print("---------------------------------------------------------------------------\n" +
                        "Submarines occupy " + submarines + " squares\n" +
                        "Destroyers occupy " + destroyers + " squares\n" +
                        "Battleships occupy " + battleships + " squares\n" +
                        "Carriers occupy " + carriers + " square\n" +
                        "---------------------------------------------------------------------------\n");
            }
            else {
                out.println("Player " + name + " used a special action");
            }
            sTime--;
        }
        else {
            playOneTurnV2(enemyBoard, enemyView, enemyName);
        }
    }
}