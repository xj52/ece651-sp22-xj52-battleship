package edu.duke.xj52.battleship;

/**
 * This class makes ships of version 2.
 */
public class V2ShipFactory implements AbstractShipFactory<Character> {

    /**
     * creates different types of ships.
     * @param where is the location and orientation of the ship.
     * @param w is the width of the ship.
     * @param h is the height of the ship.
     * @param letter is the character representing the ship.
     * @param name is the name of the ship.
     * @return a specific instance of a ship.
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if ((name.equals("Submarine") || name.equals("Destroyer")) && where.orientation == 'V') {
            return new RectangleShip<>(name, where.where, w, h, letter, '*', where.orientation);
        }
        else if ((name.equals("Submarine") || name.equals("Destroyer")) && where.orientation == 'H') {
            return new RectangleShip<>(name, where.where, h, w, letter, '*', where.orientation);
        }
        else if (name.equals("Submarine") || name.equals("Destroyer")) {
            throw new IllegalArgumentException("The orientation of Submarine and Destroyer should be H or V\n");
        }
        else if ((name.equals("Carrier") || name.equals("Battleship")) &&
                (where.orientation == 'U' || where.orientation == 'L' || where.orientation == 'R' || where.orientation == 'D')) {
            return new SpecialShapedShip<>(name, where.where, where.orientation, letter, '*');
        }
        else {
            throw new IllegalArgumentException("The orientation of Carrier and Battleship should be U or L or R or D\n");
        }
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 0, 0, 'b', "Battleship"); // width and height equal to 0 because special-shaped ships don't need them
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 0, 0, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
