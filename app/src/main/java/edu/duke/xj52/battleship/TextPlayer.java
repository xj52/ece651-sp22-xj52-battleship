package edu.duke.xj52.battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;
import java.util.Random;

/**
 * This class implements a player's choice for the ships.
 */
public class TextPlayer {
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    protected boolean isComputer;
    Random random = new Random(1);

    /**
     * Constructs a text player with the specified board, input stream, output stream, ship factory, and name.
     * @param theBoard is a Board character.
     * @param inputReader is the input stream.
     * @param out is output stream.
     * @param shipFactory is the ship factory.
     * @param name is the name of the player.
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        this.isComputer = false;
        setupShipCreationMap();
        setupShipCreationList();
    }

    /**
     * Reads information from players
     * @param prompt is the prompt for players, telling what to do
     * @return a Placement object.
     * @throws IOException
     */
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("There is not new line to read.");
        }
        return new Placement(s);
    }

    /**
     * generates a valid random coordinate.
     * @return a string.
     */
    public String randomCoordinate() {
        int col = random.nextInt(theBoard.getWidth());
        int row = random.nextInt(theBoard.getHeight());
        char r = (char) (row + 'A');
        char c = (char) (col + '0');
        return String.valueOf(r) + String.valueOf(c);
    }

    /**
     * generates a valid random placement.
     * @param shipName is the name of the ship.
     * @return a string.
     */
    public String randomPlacement(String shipName) {
        char ori;
        if (shipName.equals("Submarine") || shipName.equals("Destroyer")) {
            int oriSD = random.nextInt(2);
            if (oriSD == 0) ori = 'V';
            else ori = 'H';
        }
        else { // if (shipName.equals("Battleship") || shipName.equals("Carrier"))
            int oriBC = random.nextInt(4);
            if (oriBC == 0) ori = 'U';
            else if (oriBC == 1) ori = 'R';
            else if (oriBC == 2) ori = 'D';
            else ori = 'L';
        }
        return randomCoordinate() + String.valueOf(ori);
    }

    /**
     * places one ship to the board and display the updated board
     * @param shipName is the name of the ship.
     * @param createFn is the function to create a specified ship.
     * @throws IOException
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        Placement p;
        if (isComputer) {
            String place = randomPlacement(shipName);
            p = new Placement(place);
        }
        else {
            p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
        }
        Ship<Character> s = createFn.apply(p);
        theBoard.tryAddShip(s);
        if (!isComputer) {
            out.print(view.displayMyOwnBoard());
        }
    }

    /**
     * Displays the prompt and related information for the players.
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        if (!isComputer) {
            out.println(view.displayMyOwnBoard());
            String start = "--------------------------------------------------------------------------------\n" +
                    "Player " + name + ": you are going to place the following ships (which are all\n" +
                    "rectangular). For each ship, type the coordinate of the upper left\n" +
                    "side of the ship, followed by either H (for horizontal) or V (for\n" +
                    "vertical).  For example M4H would place a ship horizontally starting\n" +
                    "at M4 and going to the right.  You have\n" + "\n" +
                    "2 \"Submarines\" ships that are 1x2\n" +
                    "3 \"Destroyers\" that are 1x3\n" +
                    "3 \"Battleships\" that are 1x4\n" +
                    "2 \"Carriers\" that are 1x6\n" +
                    "--------------------------------------------------------------------------------\n";
            out.println(start);
        }
        for (String s: shipsToPlace) {
            this.doOnePlacement(s, shipCreationFns.get(s));
        }
    }

    /**
     * makes the ship name correspond to the correct make-ship function.
     */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    }

    /**
     * defines the required number of each kind of ship.
     */
    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /**
     * plays one turn for each player.
     * @param enemyBoard is the board of enemy.
     * @param enemyView is the board text view of enemy.
     * @param enemyName is enemy's name.
     * @return false if the player is the winner, otherwise true.
     * @throws IOException
     */
    public boolean playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        if (theBoard.CheckLose()) {
            out.println("The winner is Player " + enemyName + "!");
            return false;
        }
        out.println("Player " + name + "'s turn:");
        String enemyHeader = "Player" + enemyName + "'s ocean";
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", enemyHeader));
        String location = inputReader.readLine();
        if (location == null) {
            throw new EOFException("There is not new line to read.");
        }
        Ship<Character> s = enemyBoard.fireAt(new Coordinate(location));
        if (s == null) {
            out.println("You missed!");
        }
        else {
            out.println("You hit a " + s.getName() + "!");
        }
        return true;
    }

    /**
     * reads a line, checks if it is null and makes it to upper case.
     * @return a string
     * @throws IOException
     */
    public String readLineAndCheckNull() throws IOException {
        String res = inputReader.readLine();
        if (res == null) {
            throw new EOFException("There is not new line to read.");
        }
        res = res.toUpperCase();
        return res;
    }

    /**
     * chooses to play by human or play by computer.
     * @param name is player's name.
     * @throws IOException
     */
    public void chooseComputerPhase(String name) throws IOException {
        out.println("Player " + name + ": Play by yourself or by computer? (H or h for yourself and C or c for computer)");
        String choice = readLineAndCheckNull();
        if (!choice.equals("H") && !choice.equals("C")) {
            throw new IllegalArgumentException("Your choice must be C (or c) for computer or H (or h) for yourself\n");
        }
        if (choice.equals("H")) {
            isComputer = false;
        }
        if (choice.equals("C")) {
            isComputer = true;
        }
    }

    public boolean isComputer() {
        return isComputer;
    }
}
