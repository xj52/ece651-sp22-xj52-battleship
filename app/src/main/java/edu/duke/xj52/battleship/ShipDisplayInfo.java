package edu.duke.xj52.battleship;

/**
 * This interface gives the information about the ship to display.
 */
public interface ShipDisplayInfo<T> {

    public T getInfo(Coordinate where, boolean hit);

}
