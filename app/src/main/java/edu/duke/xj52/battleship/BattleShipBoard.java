package edu.duke.xj52.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *  This class constructs a BattleShipBoard
 */
public class BattleShipBoard<T> implements Board<T>{

    private final int width;
    private final int height;
    final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    HashSet<Coordinate> enemyMisses;
    HashMap<Coordinate, Character> enemyHits;
    final T missInfo;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Constructs a BattleShipBoard with the specified width and height
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @param placementChecker is a checker for valid placement.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementChecker, T missInfo) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.myShips = new ArrayList<>();
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<>();
        this.missInfo = missInfo;
        this.enemyHits = new HashMap<>();
    }

    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new NoCollisionRuleChecker<T>(new InBoundsRuleChecker<T>(null)), missInfo);
    }

    /**
     * Adds a ship to the array list
     * @param toAdd is a ship to try to add
     * @return true if the ship is added successfully, false if invalid
     */
    public String tryAddShip(Ship<T> toAdd){
        String addRes = placementChecker.checkPlacement(toAdd, this);
        if (addRes == null) {
            myShips.add(toAdd);
            return null;
        }
        else return addRes;
    }

    /**
     * finds which (if any) ship occupies the given coordinate for self
     * @param where is a Coordinate object
     * @return a ship if found, null if not found
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * finds which (if any) ship occupies the given coordinate for enemy
     * @param where is a Coordinate object
     * @return a ship if found, null if not found
     */
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    /**
     * finds which (if any) ship occupies the given coordinate
     * @param where is a Coordinate object
     * @param isSelf is true if self, otherwise false
     * @return a ship if found, null if not found
     */
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if (!isSelf) {
            if (enemyMisses.contains(where)) {
                return missInfo;
            }
            if (enemyHits.containsKey(where)) {
                return (T) enemyHits.get(where); //
            }
        }
        else {
            for (Ship<T> s: myShips) {
                if (s.occupiesCoordinates(where)){
                    return s.getDisplayInfoAt(where, isSelf);
                }
            }
        }
        return null;
    }

    /**
     * fires at a ship if the given coordinate is valid.
     * @param c is a Coordinate object.
     * @return a ship which is fired at.
     */
    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(c)) {
                s.recordHitAt(c);
                enemyMisses.remove(c);
                enemyHits.put(c, s.getName().substring(0, 1).toLowerCase().charAt(0));
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }

    /**
     * checks if loss.
     * @return false if haven't loss, true if loss.
     */
    public boolean CheckLose() {
        for (Ship<T> s: myShips) {
            if (!s.isSunk()) {
                return false;
            }
        }
        return true;
    }

    /**
     * checks if a coordinate belongs to a ship.
     * @param where is a coordinate.
     * @return a ship.
     */
    public Ship<T> belongsToShip(Coordinate where) {
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s;
            }
        }
        return null;
    }

    /**
     * deletes a ship from my list of ships.
     * @param s is a ship.
     */
    public void deleteShip(Ship<T> s) {
        myShips.remove(s);
    }

    /**
     * readds the ship after deleting it before.
     * @param s is a ship.
     */
    public void readdShip(Ship<T> s) {
        myShips.add(s);
    }

    /**
     * gives a interface to check if a placement is valid.
     * @return an object of PlacementRuleChecker.
     */
    public PlacementRuleChecker<T> getPlacementChecker() {
        return placementChecker;
    }

    /**
     * gets the myShips field.
     * @return an array list of my ships.
     */
    public ArrayList<Ship<T>> getMyShips() {
        return myShips;
    }
}
