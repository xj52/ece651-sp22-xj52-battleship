package edu.duke.xj52.battleship;

/**
 * This class handles placements in the board
 * It supports two ways to read placements:
 * one for coordinate and orientation, and one for strings.
 */
public class Placement {
    final Coordinate where;
    final char orientation;

    public Coordinate getWhere() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }

    /**
     * Constructs a placement object by giving a Coordinate object and a char.
     * @param where is a Coordinate object.
     * @param orientation is a char representing the orientation of the ship.
     */
    public Placement(Coordinate where, char orientation) {
        this.where = where;
        String ori = String.valueOf(orientation);
        String str = ori.toUpperCase();
        this.orientation = str.charAt(0);
    }

    /**
     * Constructs a placement object by giving a string.
     * @param descr is the placement string to parse.
     * @throws IllegalArgumentException if the string is longer than 3 or the format of string is invalid.
     */
    public Placement(String descr) {
        if(descr.length() != 3) {
            throw new IllegalArgumentException("That placement is invalid: Placement's string length must be 3 but is " + descr.length());
        }
        String coor = descr.substring(0, 2);
        this.where = new Coordinate(coor);
        String ori = descr.substring(2);
        String str = ori.toUpperCase();
        char oriLetter = str.charAt(0);
        if(oriLetter != 'H' && oriLetter != 'V' && oriLetter != 'U' && oriLetter != 'D' && oriLetter != 'L' && oriLetter != 'R') {
            throw new IllegalArgumentException("That placement is invalid: Placement's orientation must be H (for horizontal) or V (for vertical) but is " + oriLetter);
        }
        this.orientation = oriLetter;
    }

    /**
     * Checks if 2 objects are equal.
     * @param o is the object to check.
     */
    @Override
    public boolean equals(Object o){
        if (o.getClass().equals(getClass())) {
            Placement c = (Placement) o;
            String cOri = String.valueOf(c.orientation);
            String str = cOri.toUpperCase();
            char cOriLetter = str.charAt(0);
            return where.equals(c.where) && orientation == cOriLetter;
        }
        return false;
    }

    /**
     * Generates a hashcode using strings' hashcode so that
     * the placement object can be stored in hashsets or hashmaps
     */
    @Override
    public String toString() {
        return "(" + where + ", " + orientation + ")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
