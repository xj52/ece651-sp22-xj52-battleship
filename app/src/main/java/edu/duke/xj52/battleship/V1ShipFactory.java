package edu.duke.xj52.battleship;

/**
 * This class makes ships of version 1.
 */
public class V1ShipFactory implements AbstractShipFactory<Character> {

    /**
     * creates different types of ships.
     * @param where is the location and orientation of the ship.
     * @param w is the width of the ship.
     * @param h is the height of the ship.
     * @param letter is the character representing the ship.
     * @param name is the name of the ship.
     * @return a specific instance of a ship.
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        RectangleShip<Character> res;
        if (where.orientation == 'V') {
            res = new RectangleShip<>(name, where.where, w, h, letter, '*', where.orientation);
        }
        else if (where.orientation == 'H'){
            res = new RectangleShip<>(name, where.where, h, w, letter, '*', where.orientation);
        }
        else {
            throw new IllegalArgumentException("The orientation of the ship should be H or V\n");
        }
        return res;
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
