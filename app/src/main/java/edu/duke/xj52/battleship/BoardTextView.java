package edu.duke.xj52.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board it will display.
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    /**
     * This displays the text view of a board
     * @return a string displaying the board
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder();
        ans.append(makeHeader());

        for (int row = 0; row < toDisplay.getHeight(); row++) {
            char letter = (char) (row + 'A');
            ans.append(letter);
            ans.append(" ");
            String sep="";
            for (int col = 0; col < toDisplay.getWidth(); col++) {
                ans.append(sep);
                Coordinate c = new Coordinate(row, col);
                if (getSquareFn.apply(c) == null) {
                    ans.append(" ");
                }
                else {
                    ans.append(getSquareFn.apply(c));
                }
                sep = "|";
            }
            ans.append(" ");
            ans.append(letter);
            ans.append("\n");
        }
        ans.append(makeHeader());
        return ans.toString(); //this is a placeholder for the moment
    }

    /**
     * This displays the text view of my own board
     * @return a string displaying my own board
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    /**
     * This displays the text view of enemy's board
     * @return a string displaying the enemy board
     */
    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    public void addSpace(int num, StringBuilder ans) {
        for (int i = 0; i < num; i++) {
            ans.append(" ");
        }
    }

    /**
     * display my board with enemy board next to it.
     * @param enemyView is the text view of the enemy
     * @param myHeader is the title of my board
     * @param enemyHeader is the title of enemy's board
     * @return the text view of my board with enemy board next to it
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String [] enemyLines = enemyView.displayEnemyBoard().split("\n");
        String [] myLines = this.displayMyOwnBoard().split("\n");
        StringBuilder ans = new StringBuilder();
        ans.append("     ");
        ans.append(myHeader);
        int spaceNumHeader = 2 * toDisplay.getWidth() + 22 - 5 - myHeader.length();
        addSpace(spaceNumHeader, ans);
        ans.append(enemyHeader);
        ans.append("\n");
        int spaceNumBody = 16;
        ans.append(myLines[0]);
        addSpace(spaceNumBody + 2, ans);
        ans.append(enemyLines[0]);
        ans.append("\n");
        for (int i = 1; i <= enemyLines.length - 2; i++) {
            ans.append(myLines[i]);
            addSpace(spaceNumBody, ans);
            ans.append(enemyLines[i]);
            ans.append("\n");
        }
        ans.append(myLines[myLines.length - 1]);
        addSpace(spaceNumBody + 2, ans);
        ans.append(enemyLines[enemyLines.length - 1]);
        ans.append("\n");
        return ans.toString();
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

}
