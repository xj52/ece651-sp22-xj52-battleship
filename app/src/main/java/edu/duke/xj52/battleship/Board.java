package edu.duke.xj52.battleship;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * an interface to get width and get height
 */
public interface Board<T> {

    public int getWidth();

    public int getHeight();

    public String tryAddShip(Ship<T> toAdd);

    public T whatIsAtForSelf(Coordinate where);

    public T whatIsAtForEnemy(Coordinate where);

    public Ship<T> fireAt(Coordinate c);

    public boolean CheckLose();

    public Ship<T> belongsToShip(Coordinate where);

    public void deleteShip(Ship<T> s);

    public void readdShip(Ship<T> s);

    public PlacementRuleChecker<T> getPlacementChecker();

    public ArrayList<Ship<T>> getMyShips();

}
