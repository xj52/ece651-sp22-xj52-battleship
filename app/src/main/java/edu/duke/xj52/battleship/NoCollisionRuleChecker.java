package edu.duke.xj52.battleship;

/**
 * This class ensures that a ship is not colliding with another ship.
 */
public class NoCollisionRuleChecker<T> extends InBoundsRuleChecker<T> {

    /**
     * check if the given ship is colliding with another ship.
     * @param theShip is a ship wanted to be checked.
     * @param theBoard is a battle board.
     * @return null if valid, information about the error if invalid.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(c) != null) {
                return "That placement is invalid: the ship overlaps another ship.";
            }
        }
        return null;
    }

    /**
     * constructs an object of NoCollisionRule.
     * @param next is the next rule to check.
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
