package edu.duke.xj52.battleship;

import java.util.HashMap;
import java.util.HashSet;

/**
 * This class constructs a special-shaped ship which
 * inherits from class BasicShip
 */
public class SpecialShapedShip<T> extends BasicShip<T> {

    final String name;

    /**
     * implements a for loop to add elements to the coordinates set.
     * @param start is the start index of a for loop.
     * @param length is the end of a for loop.
     * @param rowIndex is the index of the row.
     * @param colIndex  is the index of the row.
     * @param coordinates is a hash set of coordinates.
     */
    static void makeCoordsHelper(int start, int length, int rowIndex, int colIndex, HashSet<Coordinate> coordinates) {
        for (int i = start; i < length; i++) {
            if (colIndex == -1) { // -1 if colIndex is i
                coordinates.add(new Coordinate(rowIndex, i));
            }
            if (rowIndex == -1) { // -1 if rowIndex is i
                coordinates.add(new Coordinate(i, colIndex));
            }
        }
    }
    /**
     * constructs a hash set to include all the coordinates of a special-shaped ship.
     * @param upperLeft is the upper left coordinate of the special-shaped ship.
     * @param name is the name of the special-shaped ship.
     * @param orientation is the orientation of the special-shaped ship.
     * @return a hash set containing all the coordinates of the special-shaped ship.
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, String name, char orientation) {
        HashSet<Coordinate> coordinates = new HashSet<>();
        if (name.equals("Battleship")) {
            if (orientation == 'U') {
                coordinates.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                makeCoordsHelper(upperLeft.getColumn(), upperLeft.getColumn() + 3, upperLeft.getRow() + 1, -1, coordinates);
            }
            if (orientation == 'L') {
                coordinates.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                makeCoordsHelper(upperLeft.getRow(), upperLeft.getRow() + 3, -1, upperLeft.getColumn() + 1, coordinates);
            }
            if (orientation == 'R') {
                coordinates.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                makeCoordsHelper(upperLeft.getRow(), upperLeft.getRow() + 3, -1, upperLeft.getColumn(), coordinates);
            }
            if (orientation == 'D') {
                coordinates.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                makeCoordsHelper(upperLeft.getColumn(), upperLeft.getColumn() + 3, upperLeft.getRow(), -1, coordinates);
            }
        }
        if (name.equals("Carrier")) {
            if (orientation == 'U') {
                makeCoordsHelper(upperLeft.getRow(), upperLeft.getRow() + 4, -1, upperLeft.getColumn(), coordinates);
                makeCoordsHelper(upperLeft.getRow() + 2, upperLeft.getRow() + 5, -1, upperLeft.getColumn() + 1, coordinates);
            }
            if (orientation == 'L') {
                makeCoordsHelper(upperLeft.getColumn() + 2, upperLeft.getColumn() + 5, upperLeft.getRow(), -1, coordinates);
                makeCoordsHelper(upperLeft.getColumn(), upperLeft.getColumn() + 4, upperLeft.getRow() + 1, -1, coordinates);
            }
            if (orientation == 'R') {
                makeCoordsHelper(upperLeft.getColumn() + 1, upperLeft.getColumn() + 5, upperLeft.getRow(), -1, coordinates);
                makeCoordsHelper(upperLeft.getColumn(), upperLeft.getColumn() + 3, upperLeft.getRow() + 1, -1, coordinates);
            }
            if (orientation == 'D') {
                makeCoordsHelper(upperLeft.getRow(), upperLeft.getRow() + 3, -1, upperLeft.getColumn(), coordinates);
                makeCoordsHelper(upperLeft.getRow() + 1, upperLeft.getRow() + 5, -1, upperLeft.getColumn() + 1, coordinates);
            }
        }
        return coordinates;
    }

    /**
     * give corresponding numbers for all the coordinates of a ship.
     * @param upperLeft is the upper left coordinate of the special-shaped ship.
     * @param name is the name of the special-shaped ship.
     * @param orientation is the orientation of the special-shaped ship.
     * @return a hash map containing all the coordinates of the special-shaped ship and the corresponding numbers.
     */
    static HashMap<Coordinate, Integer> makeOrder(Coordinate upperLeft, String name, char orientation) {
        HashMap<Coordinate, Integer> order = new HashMap<>();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        if (name.equals("Battleship")) {
            if (orientation == 'U') {
                order.put(new Coordinate(row, col + 1), 1);
                order.put(new Coordinate(row + 1, col), 2);
                order.put(new Coordinate(row + 1, col + 1), 3);
                order.put(new Coordinate(row + 1, col + 2), 4);
            }
            if (orientation == 'R') {
                order.put(new Coordinate(row, col), 2);
                order.put(new Coordinate(row + 1, col), 3);
                order.put(new Coordinate(row + 1, col + 1), 1);
                order.put(new Coordinate(row + 2, col), 4);
            }
            if (orientation == 'D') {
                order.put(new Coordinate(row, col), 4);
                order.put(new Coordinate(row , col + 1), 3);
                order.put(new Coordinate(row, col + 2), 2);
                order.put(new Coordinate(row + 1, col + 1), 1);
            }
            if (orientation == 'L') {
                order.put(new Coordinate(row, col + 1), 4);
                order.put(new Coordinate(row + 1, col), 1);
                order.put(new Coordinate(row + 1, col + 1), 3);
                order.put(new Coordinate(row + 2, col + 1), 2);
            }
        }
        if (name.equals("Carrier")) {
            if (orientation == 'U') {
                order.put(new Coordinate(row, col), 1);
                order.put(new Coordinate(row + 1, col), 2);
                order.put(new Coordinate(row + 2, col), 3);
                order.put(new Coordinate(row + 2, col + 1), 5);
                order.put(new Coordinate(row + 3, col), 4);
                order.put(new Coordinate(row + 3, col + 1), 6);
                order.put(new Coordinate(row + 4, col + 1), 7);
            }
            if (orientation == 'R') {
                order.put(new Coordinate(row, col + 1), 4);
                order.put(new Coordinate(row, col + 2), 3);
                order.put(new Coordinate(row, col + 3), 2);
                order.put(new Coordinate(row, col + 4), 1);
                order.put(new Coordinate(row + 1, col), 7);
                order.put(new Coordinate(row + 1, col + 1), 6);
                order.put(new Coordinate(row + 1, col + 2), 5);
            }
            if (orientation == 'D') {
                order.put(new Coordinate(row, col), 7);
                order.put(new Coordinate(row + 1, col), 6);
                order.put(new Coordinate(row + 2, col), 5);
                order.put(new Coordinate(row + 1, col + 1), 4);
                order.put(new Coordinate(row + 2, col + 1), 3);
                order.put(new Coordinate(row + 3, col + 1), 2);
                order.put(new Coordinate(row + 4, col + 1), 1);
            }
            if (orientation == 'L') {
                order.put(new Coordinate(row, col + 2), 5);
                order.put(new Coordinate(row, col + 3), 6);
                order.put(new Coordinate(row, col + 4), 7);
                order.put(new Coordinate(row + 1, col), 1);
                order.put(new Coordinate(row + 1, col + 1), 2);
                order.put(new Coordinate(row + 1, col + 2), 3);
                order.put(new Coordinate(row + 1, col + 3), 4);
            }
        }
        return order;
    }

    /**
     * constructs a special-shaped ship.
     * @param name is the name of the special-shaped ship.
     * @param upperLeft is the upper left coordinate of the rectangle ship.
     * @param orientation is the orientation of the special-shaped ship.
     * @param myDisplayInfo is the information of my special-shaped ship.
     * @param enemyDisplayInfo is the information of enemy's rectangle ship.
     */
    public SpecialShapedShip(String name, Coordinate upperLeft, char orientation, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, name, orientation), myDisplayInfo, enemyDisplayInfo, makeOrder(upperLeft, name, orientation), orientation);
        this.name = name;
    }

    /**
     * constructs a special-shaped ship.
     * @param name is the name of the special-shaped ship.
     * @param upperLeft is the upper left coordinate of the special-shaped ship.
     * @param orientation is the orientation of the special-shaped ship.
     * @param data is the letter representation of the ship.
     * @param onHit is the on hit representation of the ship.
     */
    public SpecialShapedShip(String name, Coordinate upperLeft, char orientation, T data, T onHit) {
        this(name, upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    public String getName() {
        return name;
    }

}
