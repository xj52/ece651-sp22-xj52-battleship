package edu.duke.xj52.battleship;

/**
 * This class ensures that the placement of the ship is in bounds.
 */
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T>{

    /**
     * checks if the given placement of the ship is out of bounds.
     * @param theShip is a ship wanted to be checked.
     * @param theBoard is a battle board.
     * @return null if valid, information about the error if invalid.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (c.getRow() < 0) {
                return "That placement is invalid: the ship goes off the top of the board.";
            }
            if (c.getRow() >= theBoard.getHeight()) {
                return "That placement is invalid: the ship goes off the bottom of the board.";
            }
            if (c.getColumn() < 0) {
                return "That placement is invalid: the ship goes off the left of the board.";
            }
            if (c.getColumn() >= theBoard.getWidth()) {
                return  "That placement is invalid: the ship goes off the right of the board.";
            }
        }
        return null;
    }

    /**
     * constructs an object of InBoundsRule.
     * @param next is the next rule to check.
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
