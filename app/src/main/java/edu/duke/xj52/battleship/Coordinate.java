package edu.duke.xj52.battleship;

/**
 * This class handles coordinates in the board
 * It supports two ways to read coordinates:
 * one for numbers, and one for strings.
 */
public class Coordinate {
    private final int row;
    private final int column;

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Constructs a coordinate by giving a string.
     * @param descr is the string to parse.
     * @throws IllegalArgumentException if the string is longer than 2 or the format of string is invalid.
     */
    public Coordinate(String descr) {
        if(descr.length() != 2) {
            throw new IllegalArgumentException("Coordinate's string length must be 2 but is " + descr.length());
        }
        String str = descr.toUpperCase();
        char rowLetter = str.charAt(0);
        char columnLetter = str.charAt(1);
        if(rowLetter < 'A' || rowLetter > 'Z') {
            throw new IllegalArgumentException("Coordinate's row letter must be an alphabet but is " + rowLetter);
        }
        if(columnLetter < '0' || columnLetter > '9') {
            throw new IllegalArgumentException("Coordinate's column letter must be a number but is " + columnLetter);
        }
        this.row = rowLetter - 'A';
        this.column = columnLetter - '0';
    }

    /**
     * Checks if 2 objects are equal.
     * @param o is the object to check.
     */
    @Override
    public boolean equals(Object o){
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    /**
     * Generates a hashcode using strings' hashcode so that
     * the coordinate can be stored in hashsets or hashmaps
     */
    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
