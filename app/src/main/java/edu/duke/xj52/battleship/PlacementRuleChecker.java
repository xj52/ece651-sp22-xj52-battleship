package edu.duke.xj52.battleship;


/**
 * This class checks whether the given placement is valid
 */
public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;
    //more stuff

    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * checks if the given placement obey the given rules.
     * @param theShip is a ship wanted to be checked.
     * @param theBoard is a battle board.
     * @return null if valid, information about the error if invalid.
     */
    public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        if (checkMyRule(theShip, theBoard) != null) {
            return checkMyRule(theShip, theBoard);
        }
        //otherwise, ask the rest of the chain.
        if (next != null) {
            return next.checkPlacement(theShip, theBoard);
        }
        //if there are no more rules, then the placement is legal
        return null;
    }
}
