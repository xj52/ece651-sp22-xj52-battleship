package edu.duke.xj52.battleship;

/**
 * This class gives the simple information about the ship to display.
 */
public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {

    T myData;
    T onHit;

    /**
     * constructs the simple information about the ship to display.
     * @param myData is the representation of a type of ship.
     * @param onHit is the representation if a ship is hit.
     */
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    /**
     * gives the information according to if the given coordinate is hit.
     * @param where is a coordinate
     * @param hit is if the coordinate is hit
     * @return onHit if hit, otherwise myData.
     */
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        }
        else {
            return myData;
        }
    }


}
