package edu.duke.xj52.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T>{

    protected HashMap<Coordinate, Boolean> myPieces; //true if is hit, false if not hit
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    protected HashMap<Coordinate, Integer> order;
    final char orientation;

    /**
     * constructs a basic ship.
     * @param where is an iterable set of coordinates.
     * @param myDisplayInfo contains the type of ship and if it is hit.
     */
    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, HashMap<Coordinate, Integer> order, char orientation) {
        myPieces = new HashMap<Coordinate, Boolean>(); // can't be omitted
        for (Coordinate c : where) {
            myPieces.put(c, false);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        this.order = order;
        this.orientation = orientation;
    }

    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.get(where) != null;
    }

    @Override
    public boolean isSunk() {
        for (Coordinate c : myPieces.keySet()) {
            if (!myPieces.get(c)) return false;
        }
        return true;
    }

    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);
        if (myShip) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        else {
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
        }
    }

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    /**
     * Checks if the provided coordinate is part of the ship.
     * @param c is the provided coordinate.
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (myPieces.get(c) == null) {
            throw new IllegalArgumentException("The provided coordinate is not part of this ship\n");
        }
    }

    @Override
    public HashMap<Coordinate, Boolean> getMyPieces() {
        return myPieces;
    }

    @Override
    public HashMap<Coordinate, Integer> getOrder() {
        return order;
    }
}
